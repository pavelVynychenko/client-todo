import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import * as moment from 'moment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  posts?:any[] = [];

  constructor(private userService: UserService) { }

  public getTimeFromNow(time:Date): string { 
    return moment(time).fromNow();
  }

  ngOnInit(): void {
    this.userService.getPublicContent().subscribe(
      data => {
        this.posts = JSON.parse(data);
      },
      err => {
        this.posts = JSON.parse(err.error);
      }
    );
  }
}