import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable, Subject, BehaviorSubject } from 'rxjs'
import { Deal } from '../models/deal.model'

const API_URL = 'http://localhost:4000/api/'

@Injectable({
  providedIn: 'root'
})
export class TodoListService {
  private dealsSource = new  BehaviorSubject(this.deals)

  constructor(private http: HttpClient) {
    this.dealsSource.subscribe((deals) => this.deals = deals)
  }

  public deals?:any = []

  currentDeals = this.dealsSource.asObservable();

  setDeals(deals: any) {
    this.dealsSource.next(deals)
  }

  addDeal(deal: any) {
    this.dealsSource.next([...this.deals, deal])
  }

  getAll(): Observable<any> {
    return this.http.get(`${API_URL}deals`)
  }

  get(id: any): Observable<any> {
    return this.http.get(`${API_URL}deal/${id}`)
  }

  create(data: any): Observable<any> {
    return this.http.post(`${API_URL}deal`, data)
  }

  update(data: Deal): Observable<any> {
    return this.http.patch(`${API_URL}deal`, data)
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${API_URL}deal/${id}`)
  }

  findByTitle(title: string): Observable<any> {
    return this.http.get(`${API_URL}deals?title=${title}`);
  }

  findByStatus(status: string): Observable<any> {
    return this.http.get(`${API_URL}deals?status=${status}`);
  }
}
