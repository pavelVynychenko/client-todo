import { Component, OnInit } from '@angular/core'
import { TodoListService } from '../../../../_services/todo-list.service'

@Component({
  selector: 'app-status-filter',
  templateUrl: './status-filter.component.html',
  styleUrls: ['./status-filter.component.scss']
})
export class StatusFilterComponent implements OnInit {

  selectedStatus: string = '';

    statusList = [
        { id: 1, name: 'To do', value: 'todo' },
        { id: 2, name: 'Doing', value: 'doing' },
        { id: 3, name: 'Done', value: 'done' },
        { id: 4, name: 'Archived', value: 'archived' },
    ];

  constructor(private todoListService: TodoListService) { }

  ngOnInit(): void {
  }

  onChange(newStatus: any) {
    console.log(newStatus)
    this.selectedStatus = newStatus;

    this.todoListService.findByStatus(this.selectedStatus)
    .subscribe(
      data => {
        console.log(data);
        this.todoListService.setDeals(data);
      },
      error => {
        console.log(error);
      }
    )
  }
}
