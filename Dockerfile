FROM node:12-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm install -g @angular/cli @angular-devkit/build-angular && npm install
COPY . ./
RUN npm run build


FROM nginx:stable-alpine
COPY --from=build /app/dist/angular-app /usr/share/nginx/html
COPY nginx/production.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]