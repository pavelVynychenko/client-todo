import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core'
import { TokenStorageService } from './_services/token-storage.service'
import { ModalService } from './modules/modal/modal.service'
import { TodoListService } from './_services/todo-list.service'

import { fromEvent } from 'rxjs'
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  name?: string;
  searchTaskTitle: string = ''; //!todo no longer used

  constructor(
    private tokenStorageService: TokenStorageService,
    private modalService: ModalService,
    private todoListService: TodoListService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken()

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser()
      this.roles = user.roles ? user.roles : 'user'
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN')
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR')
      this.name = user.name;
    }

    const searchBox = document.getElementById('deals-search-input') as HTMLInputElement
    fromEvent(searchBox, 'input').pipe(
      map(e => (e.target as HTMLInputElement).value),
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(searchTerm => this.todoListService.findByTitle(searchTerm))
    ).subscribe(
      data => {
        this.todoListService.setDeals(data)
      },
      error => {
        console.log(error)
      })
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
      this.modalService.close(id);
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
