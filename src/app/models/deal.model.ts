export class Deal {
  id?: number;
  title?: string;
  content?: string;
  status?: string;
  priority?: string;
}