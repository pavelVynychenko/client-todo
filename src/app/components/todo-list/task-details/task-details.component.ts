import { Component, OnInit } from '@angular/core'
import { TodoListService } from '../../../_services/todo-list.service'
import { ActivatedRoute, Router } from '@angular/router'
import { Deal } from '../../../models/deal.model'

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent implements OnInit {
  currentDeal: Deal = {};
  statusList: any[] = ['todo', 'doing', 'done']
  priorityList: any[] = ['low', 'medium', 'high']
  isFailed = false
  errorMessage = ''
  // !todo testing data sharing
  test:any = ''

  constructor(
    private todoListService: TodoListService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.getDeal(this.route.snapshot.paramMap.get('id'))
    // !todo testing data sharing
    this.todoListService.currentDeals.subscribe(message => (this.test= message))
    console.log('nice data from subject', this.test)
  }

  getDeal(id: any): void {
    this.todoListService.get(id)
      .subscribe(
        data => {
          this.currentDeal = data
          console.log(data)
        },
        err => {
          console.log(err)
        });
  }

  updateDeal(): void {
    const updatingDeal = {
      title: this.currentDeal.title,
      content: this.currentDeal.content,
      status: this.currentDeal.status,
      priority: this.currentDeal.priority,
      id: this.currentDeal.id
    }
    this.todoListService.update(updatingDeal)
    .subscribe(
      response => {
        console.log(response)
        this.router.navigate(['/tasks'])
      },
      err => {
        this.errorMessage = err.error
        this.isFailed = true
      }
    )
  }

  deleteDeal(): void {
    this.todoListService.delete(this.currentDeal.id)
    .subscribe(
      response => {
        console.log(response)
        this.router.navigate(['/tasks'])
      },
      err => {
        this.errorMessage = err.error
        this.isFailed = true
      }
    )
  }
}
