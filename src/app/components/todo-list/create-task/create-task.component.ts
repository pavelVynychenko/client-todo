import { Component, OnInit } from '@angular/core'
import { TodoListService } from '../../../_services/todo-list.service'
import { TokenStorageService } from '../../../_services/token-storage.service'

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {
  form: any = {
    title: null,
    content: null,
    status: null,
    priority: null,
    userId: null,
  };
  userId: number | null = null
  isSuccessful = false
  isFailed = false
  errorMessage = ''
  successMessage = ''
  statusList: any[] = ['todo', 'doing', 'done']
  priorityList: any[] = ['low', 'medium', 'high']

  constructor(
    private todoListService: TodoListService,
    private tokenStorageService: TokenStorageService
  ) { }

  ngOnInit(): void {
    const user = this.tokenStorageService.getUser()
    this.userId = user.id
    this.form.userId = this.userId
  }

  onSubmit(): void {

    this.todoListService.create(this.form)
    .subscribe(
      deal => {
        console.log(deal)
        this.todoListService.addDeal(deal)
        this.isSuccessful = true
        this.successMessage = 'Task was created'
        this.isFailed = false
      },
      err => {
        this.errorMessage = err.error
        this.isFailed = true
      }
    )
  }
}