import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from './modules/modal';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardModeratorComponent } from './components/board-moderator/board-moderator.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { TodoListComponent } from './components/todo-list/get-all/_get-all.component';
import { CreateTaskComponent } from './components/todo-list/create-task/create-task.component'
import { TaskDetailsComponent } from './components/todo-list/task-details/task-details.component';
import { StatusFilterComponent } from './components/todo-list/filters/status-filter/status-filter.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    TodoListComponent,
    CreateTaskComponent,
    TaskDetailsComponent,
    StatusFilterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModalModule,
    NgSelectModule,
  ],
  exports: [
    CreateTaskComponent,
    StatusFilterComponent
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
