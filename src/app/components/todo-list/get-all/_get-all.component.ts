import { Component, OnInit } from '@angular/core'
import { TodoListService } from '../../../_services/todo-list.service'
import { Deal } from '../../../models/deal.model'

@Component({
  selector: 'app-todo-list-get-all',
  templateUrl: './_get-all.component.html',
  styleUrls: ['./_get-all.component.scss']
})
export class TodoListComponent implements OnInit {

  deals?:Deal[] = []
  currentDeal: Deal = {}
  currentIndex: number = -1

  constructor(private todoListService: TodoListService) { }

  ngOnInit(): void {
    this.todoListService.getAll().subscribe(
      data => {
        this.todoListService.setDeals(data)
      },
      err => {
        this.deals = JSON.parse(err.error).message
      }
    )
    this.todoListService.currentDeals.subscribe(deals => (this.deals = deals))
  }

  setActiveDeal(deal: Deal, index: number): void {
    this.currentDeal = deal;
    this.currentIndex = index;
  }
}
